/**
 * Define template to be preloaded in the main entry file
 */
export const preloadHandlebarsTemplates = async () => {

    // Paths to load
    const templatePaths = [
        "systems/ttb-foundryvtt/templates/actor-sheet.html",
        "systems/ttb-foundryvtt/templates/item-sheet.html"
    ];

    // Return the functionm call on the defined paths
    return loadTemplates(templatePaths);
}